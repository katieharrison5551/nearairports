//
//  NetworkingClient.swift
//  NearAirports
//
//  Created by MohammadReza on 9/1/21.
//

import Foundation
import Alamofire

class NetworkingClient {

    static let shared : NetworkingClient = {
        let instance = NetworkingClient()
        return instance
    }()

    typealias getResponse = (Data?, Error?) -> Void
    typealias postResponse = (Data?, Error?) -> Void

    func getRequest(_ url: URLConvertible, parameters: [String : Any]? = nil, headers : HTTPHeaders? = nil, completion: @escaping getResponse) {
        AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers, interceptor: nil).validate().responseData { (response) in
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            if let error = response.error {
                completion(nil, error)
            } else if let data = response.data {
                completion(data, nil)
            }
        }
    }

    func postRequest(_ url: URLConvertible, parameters: [String : Any]? = nil, headers: HTTPHeaders? = nil, completion: @escaping postResponse) {
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers, interceptor: nil) { (urlRequest) in
            urlRequest.timeoutInterval = 5
        }.validate().responseData { (response) in
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            if let error = response.error {
                completion(response.data, error)
            } else if let data = response.data {
                completion(data, nil)
            }
        }
    }
}
