//
//  BaseSDK.swift
//  NearAirports
//
//  Created by MohammadReza on 9/1/21.
//

import Foundation
import Alamofire

class BaseSDK: ResponseValidationProtocol  {

    let baseURLstring = "https://test.api.amadeus.com/v1"
    let decoder = JSONDecoder()

    func makeURL(url: String) -> URL {
        return URL(string: baseURLstring + url)!
    }

    func getBearerToken() -> String? {
        guard let token = Utility().reteriveData(inUserDefulat: "AccessToken") as? String else{
            return nil
        }
        return token
    } 

    func getHeader() -> HTTPHeaders? {
        if let token = getBearerToken() {
            return [.authorization(bearerToken: token)]
        } else {
            return [.accept("application/json")]
        }
    }

}
