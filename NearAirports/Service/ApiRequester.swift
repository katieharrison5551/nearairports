//
//  ApiRequester.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

typealias apiServiceCompletion<T: Decodable> = ((T?, ErrorModel?) -> Void)

protocol ApiRequestServiceProtocol {
    func apiRequest<T: Decodable>(parameter: Dictionary<String, Any>, stringURL: String, targetModel: T.Type, withAuth: Bool, method: Method, completion: @escaping apiServiceCompletion<T>)
}

class apiRequester: BaseSDK {
    //
}

extension apiRequester: ApiRequestServiceProtocol {

    func apiRequest<T: Decodable>(parameter: Dictionary<String, Any>, stringURL: String, targetModel: T.Type, withAuth: Bool, method: Method, completion: @escaping apiServiceCompletion<T>) {

        switch method {
        case .post:
            NetworkingClient.shared.postRequest(makeURL(url: stringURL), parameters: parameter, headers: withAuth ? getHeader() : nil) {
                data, error in

                self.handleResponse(response: data, model: targetModel.self) { response in
                    completion(response.responseModel, nil)
                }
            }
        case .get:
            NetworkingClient.shared.getRequest(makeURL(url: stringURL), parameters: parameter, headers: withAuth ? getHeader() : nil) {
                data, error in

                self.handleResponse(response: data, model: targetModel.self) { response in
                    completion(response.responseModel, nil)
                }
            }
        }


    }
}
