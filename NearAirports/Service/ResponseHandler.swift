//
//  ResponseHandler.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

typealias ResponseCompletion<T> = ((ResponseValidation<T>) -> Void)

struct ResponseValidation<T> {
    let responseModel: T?
    let errorModel: ErrorModel?
}

protocol ResponseValidationProtocol {
    func handleResponse<T: Decodable>(response: Data?, model: T.Type, completion: @escaping ResponseCompletion<T>)
}

extension ResponseValidationProtocol {

    func handleResponse<T: Decodable>(response: Data?, model: T.Type, completion: @escaping ResponseCompletion<T>) {
        
        do {
            if let data = response {
                let decoded = try JSONDecoder().decode(model.self, from: data)
                completion(ResponseValidation<T>(responseModel: decoded, errorModel: nil))
            }
        } catch {
            completion(ResponseValidation<T>(responseModel: nil, errorModel: ErrorModel(title: "error".getString(), message: "failedDecodeJson".getString())))
        }
    }
}
