//
//  AirportTableViewCell.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import UIKit

class AirportTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameCityLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var iataLabel: UILabel!
    @IBOutlet weak var timeZoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillCell(airportData: SingleAirport) {
        nameCityLabel.text = airportData.nameCity
        distanceLabel.text = airportData.distance
        iataLabel.text = airportData.iata
        timeZoneLabel.text = airportData.timeZone
    }
    
}
