//
//  AirportsViewController.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import UIKit
import PKHUD
import RxSwift
import RxCocoa

class AirportsViewController: UIViewController {

//MARK: - Views
    
    @IBOutlet weak var airportsTableView: UITableView!

//MARK: - init var
    var userCoordinate: CoordinateModel!
    private var airportsViewModel: AirportsListViewModel!
    private let airportCellIdentifier = "airportIdentifer"
    private let disposeBag = DisposeBag()
    private var airportList = PublishSubject<[SingleAirport]>()
    private var isLoadingList = false
    private var sort: String!

//MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initilize()
    }

//MARK: - Initilize
    fileprivate func initilize() {
        airportsViewModel = AirportsListViewModel()
        airportsTableView.register(UINib(nibName: "AirportTableViewCell", bundle: nil), forCellReuseIdentifier: airportCellIdentifier)
        setupAirportListBinding()
        setupTableViewBinding()
        setupIsLoadingViewBinding()
        getAirportsList(sort: sort, isPaginating: false)
    }

//MARK: - Api
    fileprivate func getAirportsList(sort: String?, isPaginating: Bool) {
        airportsViewModel.getAirports(lat: userCoordinate.latitude, long: userCoordinate.longitude, sort: sort, isForPaginating: isPaginating)
    }

//MARK: - setup airportList binding
    fileprivate func setupAirportListBinding() {
        airportsViewModel.especializedAirports
            .subscribe(onNext: { airports in
                self.isLoadingList = false
                self.airportList.onNext(airports)
            }).disposed(by: disposeBag)
    }

    fileprivate func setupTableViewBinding() {
        airportList.bind(to: airportsTableView.rx.items(cellIdentifier: airportCellIdentifier, cellType: AirportTableViewCell.self)) { row, airport, cell in
            cell.fillCell(airportData: airport)
        }.disposed(by: disposeBag)

        airportsTableView.rx.didScroll.subscribe { [weak self] _ in
            guard let self = self else { return }
            let offSetY = self.airportsTableView.contentOffset.y
            let contentHeight = self.airportsTableView.contentSize.height

            if offSetY > (contentHeight - self.airportsTableView.frame.size.height - 100) && !self.isLoadingList {
                //self.viewModel.fetchMoreDatas.onNext(())
                self.isLoadingList = true
                if self.airportsViewModel.especializedAirports.value.count < 30 {
                    self.getAirportsList(sort: self.sort, isPaginating: true)
                }

            }
        }
        .disposed(by: disposeBag)
    }

    fileprivate func setupIsLoadingViewBinding() {
        airportsViewModel.isLoading
            .asObserver()
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { isLoading in
                isLoading ? Utility.showHudLoading() : Utility.hideSuccessHudLoading()
            }).disposed(by: disposeBag)
    }

//MARK: - Action

    @IBAction func didTapOnBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapOnFilterButton(_ sender: Any) {
        showFilterOptions()
    }


//MARK: - Setup BottomSheet for filter

    fileprivate func showFilterOptions() {
        let actionSheet = UIAlertController(title: "filter".getString(), message: "filterMessage".getString(), preferredStyle: .actionSheet)
        let relevance = UIAlertAction(title: "relevance".getString(), style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.sort = SortChoice.relevance.description
            self.getAirportsList(sort: self.sort, isPaginating: false)
        }

        let distance = UIAlertAction(title: "distance".getString(), style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.sort = SortChoice.distance.description
            self.getAirportsList(sort: self.sort, isPaginating: false)
        }

        let flightScore = UIAlertAction(title: "flightsScore".getString(), style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.sort = SortChoice.flightsScore.description
            self.getAirportsList(sort: self.sort , isPaginating: false)
        }

        let travelersScore = UIAlertAction(title: "travelersScore".getString(), style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.sort = SortChoice.travelersScore.description
            self.getAirportsList(sort: self.sort, isPaginating: false)
        }

        actionSheet.addAction(relevance)
        actionSheet.addAction(distance)
        actionSheet.addAction(flightScore)
        actionSheet.addAction(travelersScore)

        self.present(actionSheet, animated: true)
    }

}
