//
//  CoordinateViewController.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import UIKit
import RxSwift

class CoordinateViewController: UIViewController {

//MARK: - Views
    @IBOutlet weak var latitudeTextField: CustomTextField!
    @IBOutlet weak var longitudeTextField: CustomTextField!

//MARK: - Init var
    private var coordinateViewModel: CoordinateViewModel!
    private var disposeBag: DisposeBag!
    private var tokenViewModel: TokenViewModel!

//MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initilize()
    }

//MARK: - initilize
    fileprivate func initilize() {
        disposeBag = DisposeBag()
        coordinateViewModel = CoordinateViewModel()
        setupCoordinateBinding()
        tokenViewModel = TokenViewModel()
        tokenViewModel.getToken()
    }
    
//MARK: - Actions
    @IBAction func didTapOnCheckButton(_ sender: Any) {
        coordinateViewModel.getCoordinate(latitude: latitudeTextField.text, longitude: longitudeTextField.text)
    }

//MARK: - setup binding
    fileprivate func setupCoordinateBinding() {
        coordinateViewModel.inputError
            .asObserver()
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { error in
                self.showAlert(error: error)
            }).disposed(by: disposeBag)

        coordinateViewModel.userCoordinate
            .asObserver()
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { coordinate in
                self.prepareAirportListViewController(coordinate: coordinate)
            }).disposed(by: disposeBag)

    }

//MARK: - show Alert
    fileprivate func showAlert(error: ErrorModel) {
        self.showActionSheet(title: error.title, message: error.message, style: .alert, actions: [self.actionMessageOK()])
    }

//MARK: - Move on navigation
    fileprivate func prepareAirportListViewController(coordinate: CoordinateModel) {
        let airportsViewController = AppStoryboard.Airports.viewController(viewControllerClass: AirportsViewController.self)
        airportsViewController.userCoordinate = coordinate
        navigationController?.pushViewController(airportsViewController, animated: true)
    }

}
