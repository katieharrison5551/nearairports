//
//  CustomTextFielf.swift
//  NearAirports
//
//  Created by MohammadReza on 9/1/21.
//

import Foundation
import UIKit

class CustomTextField : LeftPadedTextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    fileprivate func setup() {
        backgroundColor = .white
        textColor = .black
        cornerRadius = 10
        borderColor = UIColor.systemBlue
        borderWidth = 2
        keyboardType = .default
        borderStyle = .none

    }

}
