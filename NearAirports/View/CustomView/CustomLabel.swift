//
//  CustomLabel.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation
import UIKit

class CustomLabel: UILabel {

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    fileprivate func setup() {
        textColor = .black
        font = UIFont.boldSystemFont(ofSize: 18)
    }

}
