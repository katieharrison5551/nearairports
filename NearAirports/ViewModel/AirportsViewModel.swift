//
//  AirportsViewModel.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation
import RxSwift
import RxCocoa

protocol AirportsListProtocol {
    func getAirports(lat: Double, long: Double, sort: String?, isForPaginating: Bool)
}

class AirportsListViewModel: BaseViewModel {

    var isLoading = PublishSubject<Bool>()
    var airports = BehaviorRelay<[AirportData]>(value: [])
    var especializedAirports = BehaviorRelay<[SingleAirport]>(value: [])
    var disposeBag = DisposeBag()
    var allAirportsCount = -1

    init() {
        super.init()
        prepareAirports()
    }

}

extension AirportsListViewModel: AirportsListProtocol {

    func getAirports(lat: Double, long: Double, sort: String?, isForPaginating: Bool) {

        if especializedAirports.value.count < allAirportsCount || allAirportsCount == -1 || sort != nil {
            isLoading.onNext(true)

            var parameter: InputAirportsList!
            if isForPaginating {
                parameter = InputAirportsList(latitude: lat, longitude: long, radius: 500, page_limit: 20, page_offset: especializedAirports.value.count, sort: sort)
            } else {
                parameter = InputAirportsList(latitude: lat, longitude: long, radius: 500, page_limit: 20, page_offset: 0, sort: sort)
            }

            service.apiRequest(parameter: parameter.dictionary()!, stringURL: URLs.getAirports.description, targetModel: AirportsModel.self, withAuth: true, method: .get) { response, error in

                self.isLoading.onNext(false)

                if error == nil {
                    if let airportsModel = response, let data = airportsModel.data, let meta = airportsModel.meta, let count = meta.count {
                        //handle data
                        if isForPaginating {
                            self.allAirportsCount = count
                            let moreAirports = self.airports.value + data
                            self.airports.accept(moreAirports)
                        } else {
                            self.airports.accept(data)
                        }
                    } else {
                        // handle empty
                        self.airports.accept([])
                    }

                } else {
                    //handleError
                }

            }
        }
    }

    fileprivate func prepareAirports() {
        self.airports.subscribe(onNext: { dataAirports in

                var requierdAirportData = [SingleAirport]()
                for singleAirport in dataAirports {
                    if let address = singleAirport.address, let cityName = address.cityName,
                       let airportName = singleAirport.name, let distance = singleAirport.distance,
                       let distanceValue = distance.value, let distanceUnit = distance.unit,
                       let iata = singleAirport.iataCode, let timeZoneOffset = singleAirport.timeZoneOffset {

                        let singleAirportData = SingleAirport(nameCity: airportName + ", " + cityName, distance: "\(distanceValue)" + " " + distanceUnit, iata: iata, timeZone: timeZoneOffset)

                        requierdAirportData.append(singleAirportData)
                    }
                }

                self.especializedAirports.accept(requierdAirportData)

            }).disposed(by: disposeBag)

    }
}
