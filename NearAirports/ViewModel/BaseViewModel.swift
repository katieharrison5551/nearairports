//
//  BaseViewModel.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

class BaseViewModel {

    var service: ApiRequestServiceProtocol!
    init(service: ApiRequestServiceProtocol = apiRequester()) {
        self.service = service
    }
}
