//
//  CoordinateViewModel.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation
import RxSwift

protocol GetCoordinateProtocol {
    func getCoordinate(latitude: String?, longitude: String?)
}

class CoordinateViewModel {
    var inputError = PublishSubject<ErrorModel>()
    var userCoordinate = PublishSubject<CoordinateModel>()
}

extension CoordinateViewModel: GetCoordinateProtocol {
    func getCoordinate(latitude: String?, longitude: String?) {

        guard let latString = latitude, let latDouble = Double(latString) else {
            inputError.onNext(ErrorModel(title: "error".getString(), message: "latitude".getString()))
            return
        }

        guard let longString = longitude, let longDouble = Double(longString) else {
            inputError.onNext(ErrorModel(title: "error".getString(), message: "longitude".getString()))
            return
        }

        userCoordinate.onNext(CoordinateModel(latitude: latDouble, longitude: longDouble))
    }
}
