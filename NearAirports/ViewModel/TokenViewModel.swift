//
//  TokenViewModel.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation
import RxSwift

protocol GetAccessTokenProtocol {
    func getToken()
}

class TokenViewModel: BaseViewModel {
    //
}

extension TokenViewModel: GetAccessTokenProtocol {
    func getToken() {
        
        let parameter = InputAuthModel(grant_type: "grantType".getString(), client_id: "clientId".getString(), client_secret: "clientSecret".getString())
        guard let parameterDictionary = parameter.dictionary() else {
            return
        }

        service.apiRequest(parameter: parameterDictionary, stringURL: URLs.getToken.description, targetModel: AuthModel.self, withAuth: false, method: .post) { responseModel, error in

            if let model = responseModel, let accessToken = model.access_token {
                Utility().saveData(inUserDefulat: accessToken, keyOfUserDefault: "AccessToken")
            }
        }
    }
}
