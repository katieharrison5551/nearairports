//
//  URLs.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

enum URLs {
    case getToken
    case getAirports

    var description: String {
        switch self {

        case .getToken:
            return "/security/oauth2/token"
        case .getAirports:
            return "/reference-data/locations/airports"
        }
    }
}
