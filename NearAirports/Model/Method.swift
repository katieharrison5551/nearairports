//
//  Method.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

enum Method {
    case post
    case get
}

