//
//  ErrorModel.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

struct ErrorModel {
    let title: String
    let message: String
}
