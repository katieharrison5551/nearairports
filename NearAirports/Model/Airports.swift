//
//  Airports.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

struct InputAirportsList: Codable {
    let latitude: Double
    let longitude: Double
    let radius: Int?
    let page_limit: Int?
    let page_offset: Int?
    let sort: String?

    enum CodingKeys: String, CodingKey {

        case latitude = "latitude"
        case longitude = "longitude"
        case radius = "radius"
        case page_limit = "page[limit]"
        case page_offset = "page[offset]"
        case sort = "sort"
    }
}

enum SortChoice {
    case relevance
    case distance
    case flightsScore
    case travelersScore

    var description: String {
        switch self {

        case .relevance:
            return "relevance"
        case .distance:
            return "distance"
        case .flightsScore:
            return "analytics.flights.score"
        case .travelersScore:
            return "analytics.travelers.score"
        }
    }
}

struct AirportsModel : Codable {
    let meta : Meta?
    let data : [AirportData]?

    enum CodingKeys: String, CodingKey {

        case meta = "meta"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        meta = try values.decodeIfPresent(Meta.self, forKey: .meta)
        data = try values.decodeIfPresent([AirportData].self, forKey: .data)
    }

}

struct Meta : Codable {
    let count : Int?
    let links : Links?

    enum CodingKeys: String, CodingKey {

        case count = "count"
        case links = "links"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        links = try values.decodeIfPresent(Links.self, forKey: .links)
    }

}

struct Links : Codable {
    let selfPage : String?
    let next : String?
    let last : String?

    enum CodingKeys: String, CodingKey {

        case selfPage = "self"
        case next = "next"
        case last = "last"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        selfPage = try values.decodeIfPresent(String.self, forKey: .selfPage)
        next = try values.decodeIfPresent(String.self, forKey: .next)
        last = try values.decodeIfPresent(String.self, forKey: .last)
    }

}

struct AirportData : Codable {
    let type : String?
    let subType : String?
    let name : String?
    let detailedName : String?
    let timeZoneOffset : String?
    let iataCode : String?
    let geoCode : GeoCode?
    let address : Address?
    let distance : Distance?
    let analytics : Analytics?
    let relevance : Double?

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case subType = "subType"
        case name = "name"
        case detailedName = "detailedName"
        case timeZoneOffset = "timeZoneOffset"
        case iataCode = "iataCode"
        case geoCode = "geoCode"
        case address = "address"
        case distance = "distance"
        case analytics = "analytics"
        case relevance = "relevance"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        subType = try values.decodeIfPresent(String.self, forKey: .subType)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        detailedName = try values.decodeIfPresent(String.self, forKey: .detailedName)
        timeZoneOffset = try values.decodeIfPresent(String.self, forKey: .timeZoneOffset)
        iataCode = try values.decodeIfPresent(String.self, forKey: .iataCode)
        geoCode = try values.decodeIfPresent(GeoCode.self, forKey: .geoCode)
        address = try values.decodeIfPresent(Address.self, forKey: .address)
        distance = try values.decodeIfPresent(Distance.self, forKey: .distance)
        analytics = try values.decodeIfPresent(Analytics.self, forKey: .analytics)
        relevance = try values.decodeIfPresent(Double.self, forKey: .relevance)
    }

}

struct Address : Codable {
    let cityName : String?
    let cityCode : String?
    let countryName : String?
    let countryCode : String?
    let regionCode : String?

    enum CodingKeys: String, CodingKey {

        case cityName = "cityName"
        case cityCode = "cityCode"
        case countryName = "countryName"
        case countryCode = "countryCode"
        case regionCode = "regionCode"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cityName = try values.decodeIfPresent(String.self, forKey: .cityName)
        cityCode = try values.decodeIfPresent(String.self, forKey: .cityCode)
        countryName = try values.decodeIfPresent(String.self, forKey: .countryName)
        countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode)
        regionCode = try values.decodeIfPresent(String.self, forKey: .regionCode)
    }

}

struct Analytics : Codable {
    let flights : Flights?
    let travelers : Travelers?

    enum CodingKeys: String, CodingKey {

        case flights = "flights"
        case travelers = "travelers"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        flights = try values.decodeIfPresent(Flights.self, forKey: .flights)
        travelers = try values.decodeIfPresent(Travelers.self, forKey: .travelers)
    }

}

struct Distance : Codable {
    let value : Int?
    let unit : String?

    enum CodingKeys: String, CodingKey {

        case value = "value"
        case unit = "unit"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        value = try values.decodeIfPresent(Int.self, forKey: .value)
        unit = try values.decodeIfPresent(String.self, forKey: .unit)
    }

}

struct Flights : Codable {
    let score : Int?

    enum CodingKeys: String, CodingKey {

        case score = "score"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        score = try values.decodeIfPresent(Int.self, forKey: .score)
    }

}

struct GeoCode : Codable {
    let latitude : Double?
    let longitude : Double?

    enum CodingKeys: String, CodingKey {

        case latitude = "latitude"
        case longitude = "longitude"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
    }

}

struct Travelers : Codable {
    let score : Int?

    enum CodingKeys: String, CodingKey {

        case score = "score"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        score = try values.decodeIfPresent(Int.self, forKey: .score)
    }

}
