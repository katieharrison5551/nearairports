//
//  Coordinate.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

struct CoordinateModel {
    let latitude: Double
    let longitude: Double
}
