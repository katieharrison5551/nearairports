//
//  SingleAirport.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

struct SingleAirport {
    let nameCity: String
    let distance: String
    let iata: String
    let timeZone: String
}
