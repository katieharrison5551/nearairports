//
//  AuthModel.swift
//  NearAirports
//
//  Created by MohammadReza on 9/2/21.
//

import Foundation

struct AuthModel : Codable {
    let type : String?
    let username : String?
    let application_name : String?
    let client_id : String?
    let token_type : String?
    let access_token : String?
    let expires_in : Int?
    let state : String?
    let scope : String?

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case username = "username"
        case application_name = "application_name"
        case client_id = "client_id"
        case token_type = "token_type"
        case access_token = "access_token"
        case expires_in = "expires_in"
        case state = "state"
        case scope = "scope"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        application_name = try values.decodeIfPresent(String.self, forKey: .application_name)
        client_id = try values.decodeIfPresent(String.self, forKey: .client_id)
        token_type = try values.decodeIfPresent(String.self, forKey: .token_type)
        access_token = try values.decodeIfPresent(String.self, forKey: .access_token)
        expires_in = try values.decodeIfPresent(Int.self, forKey: .expires_in)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        scope = try values.decodeIfPresent(String.self, forKey: .scope)
    }

}

struct InputAuthModel: Codable {
    let grant_type : String
    let client_id : String
    let client_secret : String

    enum CodingKeys: String, CodingKey {

        case grant_type = "grant_type"
        case client_id = "client_id"
        case client_secret = "client_secret"
    }
}

