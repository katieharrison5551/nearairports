//
//  String.swift
//  NearAirports
//
//  Created by MohammadReza on 9/1/21.
//

import Foundation

extension String {
    func getString() -> String {
        return NSLocalizedString(self, comment: self)
    }
}
